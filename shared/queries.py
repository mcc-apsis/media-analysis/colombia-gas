from collections import OrderedDict

queries = OrderedDict([
    ('Methane Removal', [
        {'qid': 'c_18', 'query': '"methane direct air capture"'},
        {'qid': 'c_19', 'query': '"methane capture"'},
        {'qid': 'c_54', 'query': 'methane AND removing AND atmosphere'},
    ]),
    ('CCS', [
        {'qid': 'c_10', 'query': '"co2 sequestration" AND storage'},
        {'qid': 'c_11', 'query': '"carbon sequestration" AND storage'},
        {'qid': 'c_12', 'query': '"carbon dioxide sequestration"'},
        {'qid': 'c_13', 'query': '"carbon capture" AND storage'},
        {'qid': 'c_14', 'query': '"carbon storage" AND capture'},
        {'qid': 'c_15', 'query': '"carbon dioxide capture" AND storage'},
        {'qid': 'c_16', 'query': '"carbon dioxide storage" AND capture'},
        {'qid': 'c_17', 'query': 'CCS AND (climate OR carbon OR co2)'},
    ]),
    ('Ocean Fertilization', [
        {'qid': 'c_20', 'query': '"ocean fertilization" OR "ocean fertilisation" '},
        {'qid': 'c_21', 'query': '"iron fertilization" OR "iron fertilisation"'},
        {'qid': 'c_22', 'query': '(fertilization OR fertilisation) '
                                 'AND (phytoplankton OR algae) '
                                 'AND (climate OR carbon OR co2)'},
        {'qid': 'c_50', 'query': '"iron seeding" AND (climate OR co2 OR carbon)'},
    ]),
    ('Ocean Alkalinization', [
        {'qid': 'c_23', 'query': '"ocean liming"'},
        {'qid': 'c_49', 'query': '"ocean alkalinity enhancement"'},
        {'qid': 'c_57', 'query': '"ocean alkalinization" OR "ocean alkalinisation"'},
    ]),
    ('Enhanced Weathering', [
        {'qid': 'c_24', 'query': '"enhanced weathering"'},
        {'qid': 'c_25', 'query': '(olivine OR basalt OR silicate) AND (co2 OR emission OR emissions)'},
        {'qid': 'c_26', 'query': 'olivine AND weathering'},
        {'qid': 'c_51', 'query': '(basalt OR silicate) AND weathering AND (co2 OR carbon OR enhanced)'},
    ]),
    ('Biochar', [
        {'qid': 'c_27','query': '(biochar OR bio-char) '
                                'AND (co2 OR carbon OR climate OR emission OR sequestration OR "greenhouse gas")'},
    ]),
    ('Afforestation/Reforestation', [
        {'qid': 'c_29', 'query': 'afforestation '
                                 'AND (climate OR co2 OR emission OR emissions OR "greenhouse gas" OR ghg OR carbon)'},
        {'qid': 'c_30', 'query': 'reforestation '
                                 'AND (climate OR co2 OR emission OR emissions OR "greenhouse gas" OR ghg OR carbon)'},
        {'qid': 'c_31', 'query': 'tree AND planting AND climate'}
    ]),
    ('Ecosystem Restoration', [
        {'qid': 'c_32', 'query': '(re-wilding OR rewilding) AND (climate OR carbon OR CO2 OR "greenhouse gas" OR GHG)'},
        {'qid': 'c_56', 'query': '("ecosystem restoration" OR "restore ecosystem") '
                                 'AND (climate OR carbon OR CO2 OR "greenhouse gas" OR GHG)'},
    ]),
    ('Soil Carbon Sequestration', [
        {'qid': 'c_33', 'query': 'soil AND sequestration AND (co2 OR carbon)'},
        {'qid': 'c_36', 'query': '"soil carbon"'},
        {'qid': 'c_37', 'query': '"carbon farming"'},
    ]),
    ('BECCS', [
        {'qid': 'c_38', 'query': 'BECCS '
                                 'AND (co2 OR carbon OR climate OR ccs OR biomass OR emission OR emissions)'},
        {'qid': 'c_39', 'query': 'biomass '
                                 'AND ("carbon capture" OR "capture carbon" OR "co2 capture" OR "capture CO2" OR ccs)'},
        {'qid': 'c_40', 'query': 'bioenergy '
                                 'AND ("carbon capture" OR "capture carbon" OR "co2 capture" OR "capture CO2" OR ccs)'},
    ]),
    ('Blue Carbon', [
        {'qid': 'c_41', 'query': 'seagrass AND (carbon OR co2)'},
        {'qid': 'c_42', 'query': 'macroalgae AND (carbon OR co2)'},
        {'qid': 'c_43', 'query': 'mangrove AND (carbon OR co2)'},
        {'qid': 'c_52', 'query': 'kelp AND (carbon OR co2)'},
        {'qid': 'c_53', 'query': '(wetland OR wetlands OR marsh OR marshes OR'
                                 ' peatland OR peatlands OR peat OR bog OR bogs) '
                                 'AND (carbon OR co2) '
                                 'AND (restore OR restoration OR rehabilitation)'},
        {'qid': 'c_44', 'query': '"blue carbon"'},
    ]),
    ('Direct Air Capture', [
        {'qid': 'c_45', 'query': 'DAC AND (climate OR carbon OR co2 OR emission OR emissions)'},
        {'qid': 'c_46', 'query': '"direct air capture"'},
        {'qid': 'c_47', 'query': '("carbon capture" OR "co2 capture") AND ("ambient air" OR "direct air")'},
        {'qid': 'c_48', 'query': 'DACCS AND (carbon OR co2 OR climate)'},
    ]),
    ('GGR (general)', [
        {'qid': 'c_09', 'query': '"methane removal"'},
        {'qid': 'c_01', 'query': '"negative emissions"'},
        {'qid': 'c_02', 'query': '"negative emission"'},
        {'qid': 'c_03', 'query': '"carbon dioxide removal"'},
        {'qid': 'c_04', 'query': '"co2 removal" AND NOT submarine AND NOT "space station"'},
        {'qid': 'c_05', 'query': '"carbon removal"'},
        {'qid': 'c_06', 'query': '"greenhouse gas removal"'},
        {'qid': 'c_07', 'query': '"ghg removal"'},
        {'qid': 'c_08', 'query': '"carbon negative" AND (climate OR co2 OR emission OR "greenhouse gas" OR ghg)'},
        {'qid': 'c_55', 'query': '(remove OR removing OR removed) AND (carbon OR co2) AND atmosphere'},
    ])
])
