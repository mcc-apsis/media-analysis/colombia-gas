import json
import logging
from enum import Enum
from pathlib import Path
from typing import Optional

import typer

from shared.lexisnexis import LexisNexis


class Mode(str, Enum):
    SEPARATE = 'SEPARATE'
    METHODS = 'METHODS'
    MERGED = 'MERGED'
    NONE = 'NONE'


QUERY = ('('
         '((subsidio* w/3 (combustible* OR gasolina OR diesel OR gas OR GLP OR petroleo)) '
         'OR (carbono w/3 (precio OR impuesto))'
         ') '
         'AND Colombia')


def main(output: Optional[Path] = None,
         progress: Optional[Path] = None,
         cont: bool = False,
         log_file: Optional[Path] = None,
         conf_file: Path = Path('conf.env'),
         filters: Optional[list[str]] = None,
         counts: bool = False,
         batch_size: int = 50,
         max_retries: int = 40,
         verbose_http: bool = False):
    if not cont and log_file is not None and log_file.exists():
        raise AssertionError(f'There already is a log file at {log_file}. '
                             f'Stopping execution here as to not accidentally overwrite existing data.')

    logging.basicConfig(format='%(asctime)s [%(levelname)s] %(name)s: %(message)s', level=logging.DEBUG,
                        filename=log_file, filemode='a' if cont else 'w')
    logger = logging.getLogger('LexisNexis')
    if verbose_http:
        logging.getLogger('httpcore').setLevel(logging.DEBUG)
        logging.getLogger('httpx').setLevel(logging.DEBUG)
    else:
        logging.getLogger('httpcore').setLevel(logging.WARNING)
        logging.getLogger('httpx').setLevel(logging.WARNING)

    # --> Estimate number of documents per CDR method
    if counts:
        lex_api = LexisNexis(conf_file=str(conf_file), max_retries=max_retries)

        cnt = lex_api.get_count(QUERY)
        logger.info(f'Found {cnt:,} documents via: {QUERY}')

        cnt = lex_api.get_count(QUERY, filters=filters)
        logger.info(f'Found {cnt:,} documents incl filters via: {QUERY}')

        # South America Z3VpZD11cm46a3JtOnB2aS1DNDMzNDJDNzA3QzM0MDE3OEI1MDAwMjlBRDlFMkRCQjtwYXJlbnRndWlkPQ
        cnt = lex_api.get_count(QUERY, filters=[
            "Geography eq 'Z3VpZD11cm46a3JtOnB2aS1DNDMzNDJDNzA3QzM0MDE3OEI1MDAwMjlBRDlFMkRCQjtwYXJlbnRndWlkPQ'"
        ])
        logger.info(f'Found {cnt:,} documents for South America via: {QUERY}')

        # Latin America Z3VpZD11cm46dG9waWM6QjRCMDQ2MTdGRTdBNDI3MkI5MkVDM0U3OTRFMkU5MTU7cGFyZW50Z3VpZD0
        cnt = lex_api.get_count(QUERY, filters=[
            "Geography eq 'Z3VpZD11cm46dG9waWM6QjRCMDQ2MTdGRTdBNDI3MkI5MkVDM0U3OTRFMkU5MTU7cGFyZW50Z3VpZD0'"
        ])
        logger.info(f'Found {cnt:,} documents for Latin America via: {QUERY}')

    # --> Download data <--
    if output is None:
        raise ValueError('Did not specify output file!')
    if progress is None:
        raise ValueError('Did not specify progress file!')

    if not cont and (output.exists() or progress.exists()):
        raise AssertionError(f'For your safety, I will never let you override the existing file {output}!')

    with LexisNexis(output_file=str(output), progress_file=str(progress),
                    conf_file=str(conf_file), max_retries=max_retries) as ln:
        logger.debug(f'Querying for {QUERY}')
        ln.set_progress_extra({})

        for item in ln.get_articles(QUERY, filters=filters, batch_size=batch_size):
            itm = item.model_dump()
            ln.append_output(json.dumps(itm, default=str) + '\n')


if __name__ == '__main__':
    typer.run(main)

# PYTHONPATH=. python scripts/lexis_colombia.py --output data/col_data.jsonl --progress data/col_progress.jsonl --log-file data/col_lexis.log --counts --cont --filters "Date le 2023-10-31"
