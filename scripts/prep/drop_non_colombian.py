import asyncio
import logging

from sqlalchemy import select, func, delete
from sqlalchemy.ext.asyncio import AsyncSession

from nacsos_data.db.schemas.items.lexis_nexis import LexisNexisItem, LexisNexisItemSource
from nacsos_data.db.schemas.items.base import Item
from nacsos_data.db.schemas.imports import m2m_import_item_table

from nacsos_data.db import get_engine_async

PROJECT_ID = '3cdb085e-0907-4325-9e44-f95b0f08c5c2'
IMPORT_ID = 'fb60f017-7ccc-4ef7-b95a-7a04afc9dc67'
SOURCES = {
    'Portafolio (Colombia)',
    'El Tiempo (Colombia)',
    'Revista Semana, Colombia',
    'El Espectador (Colombia)',
    'El Pais, Colombia',
    'El Colombiano, Colombia',
    'Diario Oficial, Colombia',
    'Superintendencia Financiera, Colombia',
    'La Patria, Colombia',
    'Q\'Hubo, Colombia',
    'El Heraldo, Colombia',
    'Revista de Economía Institucional',
    'La República (Colombia)',
    'Observatorio de Analisis de los Sistemas Internacionales (OASIS)',
    'Revista Tecnura'
}

logging.basicConfig(format='%(asctime)s [%(levelname)s] %(name)s: %(message)s', level=logging.INFO)
logger = logging.getLogger('del')
logger.setLevel(logging.DEBUG)
logger.debug('first')


async def main():
    db_engine = get_engine_async(conf_file='conf.env')
    async with (db_engine.session() as session):  # type: AsyncSession
        stmt = (select(func.count(LexisNexisItemSource.item_source_id))
                .join(m2m_import_item_table, m2m_import_item_table.c.item_id == LexisNexisItemSource.item_id)
                .where(LexisNexisItemSource.name.in_(SOURCES), m2m_import_item_table.c.import_id == IMPORT_ID))
        num_in = (await session.execute(stmt)).scalar()

        stmt = (select(func.count(LexisNexisItemSource.item_source_id))
                .join(m2m_import_item_table, m2m_import_item_table.c.item_id == LexisNexisItemSource.item_id)
                .where(LexisNexisItemSource.name.notin_(SOURCES), m2m_import_item_table.c.import_id == IMPORT_ID))
        num_out = (await session.execute(stmt)).scalar()

        logger.info(f'Included: {num_in:,}, excluded: {num_out:,}, total: {num_out + num_in:,}')

        logger.info('Fetching item ids to delete')
        stmt = (select(LexisNexisItemSource.item_id)
                .join(m2m_import_item_table, m2m_import_item_table.c.item_id == LexisNexisItemSource.item_id)
                .where(LexisNexisItemSource.name.notin_(SOURCES), m2m_import_item_table.c.import_id == IMPORT_ID))
        item_ids_del = [str(r['item_id']) for r in (await session.execute(stmt)).mappings().all()]
        logger.info(f'Going to delete data related to {len(item_ids_del):,} items')

        logger.debug('Prepping delete statements...')
        stmt_del_imp = delete(m2m_import_item_table).where(m2m_import_item_table.c.item_id.in_(item_ids_del))
        stmt_del_lns = delete(LexisNexisItemSource).where(LexisNexisItemSource.item_id.in_(item_ids_del))
        stmt_del_lni = delete(LexisNexisItem).where(LexisNexisItem.item_id.in_(item_ids_del))
        stmt_del_itm = delete(Item).where(Item.item_id.in_(item_ids_del))

        logger.debug('Executing delete statements...')
        await session.execute(stmt_del_imp)
        await session.execute(stmt_del_lns)
        await session.execute(stmt_del_lni)
        await session.execute(stmt_del_itm)

if __name__ == '__main__':
    asyncio.run(main())
