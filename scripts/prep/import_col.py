import asyncio
import logging

from sqlalchemy.ext.asyncio import AsyncSession

from nacsos_data.util.lexisnexis.importer import import_lexis_nexis
from nacsos_data.db import get_engine_async

PROJECT_ID = '3cdb085e-0907-4325-9e44-f95b0f08c5c2'
USER_ID = 'b52352db-4550-47fa-9883-816a4dda7b7e'

logging.basicConfig(format='%(asctime)s [%(levelname)s] %(name)s: %(message)s', level=logging.INFO)
logger = logging.getLogger('test')
logger.setLevel(logging.DEBUG)
logger.debug('first')


async def main():
    db_engine = get_engine_async(conf_file='conf.env')
    async with db_engine.session() as session:  # type: AsyncSession
        await import_lexis_nexis(session=session,
                                 project_id=PROJECT_ID,
                                 filename='data/col_data.jsonl',
                                 import_name='LexisNexis with Filter "Date le 2023-10-31"',
                                 user_id=USER_ID,
                                 fail_on_parse_error=False,
                                 description='Data gathered 23.11.2023\n'
                                             'Query: (((subsidio* w/3 (combustible* OR gasolina OR diesel OR gas OR GLP OR petroleo)) OR (carbono w/3 (precio OR impuesto))) AND Colombia\n'
                                             'Filter: Date le 2023-10-31',
                                 log=logger)


if __name__ == '__main__':
    asyncio.run(main())

