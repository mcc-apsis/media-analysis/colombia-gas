-- View items with source information
SELECT lis.*, li.*, i.*
FROM lexis_item_source lis
         JOIN public.item i on lis.item_id = i.item_id
         JOIN public.lexis_item li on lis.item_id = li.item_id
WHERE li.project_id = '3cdb085e-0907-4325-9e44-f95b0f08c5c2'
LIMIT 5;

-- Number of items per type
SELECT lis.meta -> 'ContentType' as tp, count(1)
FROM lexis_item_source lis
         JOIN public.item i on lis.item_id = i.item_id
         JOIN public.lexis_item li on lis.item_id = li.item_id
WHERE li.project_id = '3cdb085e-0907-4325-9e44-f95b0f08c5c2'
GROUP BY tp;

-- Number of items per source
SELECT lis.name as source, count(1) as cnt
FROM lexis_item_source lis
         JOIN public.item i on lis.item_id = i.item_id
         JOIN public.lexis_item li on lis.item_id = li.item_id
WHERE li.project_id = '3cdb085e-0907-4325-9e44-f95b0f08c5c2'
GROUP BY source
ORDER BY cnt desc;

-- Number of items per section
SELECT lis.section as section, count(1) as cnt
FROM lexis_item_source lis
         JOIN public.item i on lis.item_id = i.item_id
         JOIN public.lexis_item li on lis.item_id = li.item_id
WHERE li.project_id = '3cdb085e-0907-4325-9e44-f95b0f08c5c2'
GROUP BY section
ORDER BY cnt desc;

-- Number of items per jurisdiction
SELECT lis.jurisdiction as jurisdiction, count(1) as cnt
FROM lexis_item_source lis
         JOIN public.item i on lis.item_id = i.item_id
         JOIN public.lexis_item li on lis.item_id = li.item_id
WHERE li.project_id = '3cdb085e-0907-4325-9e44-f95b0f08c5c2'
GROUP BY jurisdiction
ORDER BY cnt desc;


-- number of items
SELECT count(distinct lis.item_id)        as num_items,
       count(distinct lis.lexis_id)       as num_lexis_ids,
       count(distinct lis.item_source_id) as num_sources
FROM lexis_item_source lis
         JOIN public.lexis_item li on lis.item_id = li.item_id
WHERE li.project_id = '3cdb085e-0907-4325-9e44-f95b0f08c5c2';


-- Number of publications by year
SELECT date_part('year', lis.published_at) yr, count(distinct lis.item_id)
FROM lexis_item_source lis
         JOIN public.lexis_item li on lis.item_id = li.item_id
WHERE li.project_id = '3cdb085e-0907-4325-9e44-f95b0f08c5c2'
GROUP BY yr
ORDER BY yr;
