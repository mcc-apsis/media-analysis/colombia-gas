# Fossil fuels in Colombia

How is the use of fossil fuels portrayed in Colombian news?

## Log

**November 2023**
* Dropped all non-Colombian news sources (included: 1,452, excluded: 2,391; `scripts/prep/drop_non_colombian.py`)
* Retrieved 3,843 articles and imported all to NACSOS (`scripts/prep/import_col.py`)
* Retrieved LexisNexis articles on query (`scripts/prep/lexis_colombia.py`, see below) 

```bash
% PYTHONPATH=. python scripts/prep/lexis_colombia.py --output data/col_data.jsonl --progress data/col_progress.jsonl --log-file data/col_lexis.log --counts --cont --filters "Date le 2023-10-31"
```